/*
 * Programmer Calculator 2021
 * Authors: Luis G. Leon Vega
 *          Daniel Moya Sanchez
 * License: MIT
 */

#include <gtest/gtest.h>

GTEST_API_ int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
