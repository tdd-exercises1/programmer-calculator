# Programmer Calculator

## Install meson

```bash
sudo apt-get install python3 python3-pip python3-setuptools \
                     python3-wheel ninja-build clang-format -y

sudo pip3 install meson
```

## Compile and install

```bash
meson buildir
# Compile
ninja -C buildir
# Compile & install
ninja -C buildir install
```

## Run tests

```bash
ninja -C buildir test
```

## Apply style

```bash
clang-format -i --style=google <FILE>
```

## Use

```bash
programmer-calculator
```

# Development

## Integrating a new test

For integrating a new test, please, create a new folder with the testcase and place your test sources.

An example structure:

```
.
├── dummy
│   ├── dummy.cpp
│   └── meson.build
```

where `dummy` is the testcase group and `dummy.cpp` is the source code of the test.

Then, integrate your testcase group to `tests/meson.build` and append `subdir('dummy')`.

## Integrating a new source code

Place your source code files to `src/` and integrate them into the file `src/meson.build`, appending the name to the list `project_source_files`. For example:

```
project_source_files += [
  'src/main.cpp'
]
```
